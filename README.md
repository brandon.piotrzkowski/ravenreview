# raven-review

A collection of codes that test the various aspects of https://git.ligo.org/lscsoft/raven/tree/master/ligo/raven and related concepts.

Recordings of calls here: https://ldas-jobs.ligo.caltech.edu/~brandon.piotrzkowski/Raven_review/call_recordings/
