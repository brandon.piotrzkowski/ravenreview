import os
import sys
import healpy as hp
import numpy as np
import pytest
from scipy import integrate

from ligo.skymap import io, distance
from ligo.skymap.tool.tests import run_entry_point

from astropy.coordinates import ICRS, SkyCoord
from astropy import units as u
from astropy_healpix import HEALPix, nside_to_level, pixel_resolution_to_nside

tmpdir = 'skymaps'
sys.stdout = open('test_skymap_overlap_output.txt','wt')


### Functions to create/combine sky maps ###
############################################

def test_combine(tmpdir, test_type):
    
    """Test ligo-skymap-combine."""
    direct = tmpdir +'/'+test_type
    
    try:
        os.makedirs(direct)
    except OSError as e:
        pass
    
    fn1 = str(direct + '/skymap1.fits.gz')
    fn2 = str(direct + '/skymap2.fits.gz')
    fn3 = str(direct + '/joint_skymap.fits.gz')

    print('Test type: '+ test_type)
    try:
        os.system('rm '+fn1)
        os.system('rm '+fn2)
        os.system('rm '+fn3)
    except:
        pass
    
    # generate a sky map
    if test_type == 'no-overlap-hemi':
        m1, nside1 = make_skymap(32, 'hemi', fn1, hemi=(1,0,0))
        m2, nside2 = make_skymap(64, 'hemi', fn2, hemi=(-1,0,0))
        print('Expected overlap integral: '+str(0))
    elif test_type == 'allsky':
        m1, nside1 = make_skymap(16, 'allsky', fn1)
        m2, nside2 = make_skymap(16, 'allsky', fn2)
        print('Expected overlap integral: '+str(1))
    elif test_type == 'allsky-hemi':
        m1, nside1 = make_skymap(32, 'allsky', fn1)
        m2, nside2 = make_skymap(64, 'hemi', fn2, hemi=(0,1,0))
        print('Expected overlap integral: '+str(1))
    elif test_type == 'same-hemi':
        m1, nside1 = make_skymap(64, 'hemi', fn1, hemi=(1,0,0))
        m2, nside2 = make_skymap(32, 'hemi', fn2, hemi=(1,0,0))
        print('Expected overlap integral: '+str(2))
    elif test_type == 'concentric-gaussians':
        ra1, ra2, dec1, dec2, error1, error2 = 180, 180, 0, 0, 10, 5
        m1, nside1 = make_skymap(16, 'cone', fn1, ra=ra1, dec=dec1, error=error1)
        m2, nside2 = make_skymap(16, 'cone', fn2, ra=ra1, dec=dec2, error=error2)
        overlap_integral(rads(ra1), rads(ra2), rads(dec1)+np.pi/2, rads(dec2)+np.pi/2,
                         rads(error1), rads(error2))
    elif test_type == 'noncoincident-gaussians':
        ra1, ra2, dec1, dec2, error1, error2 = 180, 0, 0, 45, 10, 5
        m1, nside1 = make_skymap(16, 'cone', fn1, ra=ra1, dec=dec1, error=error1)
        m2, nside2 = make_skymap(16, 'cone', fn2, ra=ra2, dec=dec2, error=error2)
        overlap_integral(rads(ra1), rads(ra2), rads(dec1)+np.pi/2, rads(dec2)+np.pi/2,
                         rads(error1), rads(error2))    
    elif test_type == 'gaussian-hemi':
        ra1, dec1, error1 = 270, 0, 1
        m1, nside1 = make_skymap(16, 'cone', fn1, ra=ra1, dec=dec1, error=error1)
        m2, nside2 = make_skymap(32, 'hemi', fn2, hemi=(0,-1,0))
        print('Expected overlap integral: '+str(2))
    
    # calculate spatial overlap integral
    skymap_overlap(m1, m2, nside1, nside2)
    
    # create combined sky map
    run_entry_point('ligo-skymap-combine', fn1, fn2, fn3)
    
    plot_skymap(fn1)
    plot_skymap(fn2)
    plot_skymap(fn3)

def skymap_overlap(m1, m2, nside1, nside2):
    if nside1 >= nside2:
        m2 = hp.ud_grade(m2, nside_out=nside1)
    else:
        m1 = hp.ud_grade(m1, nside_out=nside2)
    skymap_overlap_integral = (
            np.dot(m1, m2)
            / m1.sum() / m2.sum()
            * len(m1))
    print('Computed overlap integral: '+str(skymap_overlap_integral))
    
def from_cone(ra, dec, error):
    center = SkyCoord(ra * u.deg, dec * u.deg)
    radius = error * u.deg

    # Determine resolution such that there are at least
    # 4 pixels across the error radius.
    hpx = HEALPix(pixel_resolution_to_nside(radius / 4, round='up'),
                  'ring', frame=ICRS())
    nside = hpx.nside
    npix2 = hp.nside2npix(nside)
    ipix = np.arange(npix2)

    # Evaluate Gaussian.
    distance = hpx.healpix_to_skycoord(ipix).separation(center)
    probdensity = np.exp(-0.5 * np.square(distance / radius).to_value(
        u.dimensionless_unscaled))
    probdensity /= probdensity.sum() * hpx.pixel_area.to_value(u.steradian)
    
    return probdensity, nside
    
def make_skymap(nside, skymap_type, path, hemi=(1,0,0), ra=0, dec=0, error=10):
    
    npix2 = hp.nside2npix(nside)
    
    if skymap_type == 'hemi':
        m1 = np.zeros(npix2)
        disc_idx = hp.query_disc(nside, hemi, np.pi / 2)
        m1[disc_idx] = 1   
    elif skymap_type =='allsky':
        m1 = np.full(npix2 ,1.)
    elif skymap_type =='cone':
        m1, nside = from_cone(ra, dec, error)
    else:
        raise AssertionError
        
    m1 /= m1.sum()
    hp.write_map(path, m1, column_names=['PROBABILITY'],
                 extra_header=[('INSTRUME', 'Y1')])
    return m1, nside

def plot_skymap(path):
    os.system('ligo-skymap-plot ' +path+' -o '+path+'.png')
    
    
### Functions to calculate overlap integrals by numerical integration ###
#########################################################################

def rads(degrees):
    return degrees/360*2*np.pi

def angdist(alpha, delta, alpha0=0, delta0=0):

    return np.arccos(np.cos(delta)*np.cos(delta0)+np.sin(delta)*np.sin(delta0)*np.cos(alpha-alpha0))

def probdensity(alpha, delta, alpha0, delta0, omega0):

    return np.exp(-.5*(angdist(alpha, delta, alpha0=alpha0, delta0=delta0)/omega0)**2.0)

def probdensity_integrand(alpha, delta, alpha0, delta0, omega0):
    
    return probdensity(alpha, delta, alpha0, delta0, omega0)*np.sin(delta)

def overlap_integrand(alpha, delta, alpha0, delta0, omega0, alpha00, delta00, omega00):

    return (np.exp(-.5*(angdist(alpha, delta, alpha0=alpha0, delta0=delta0)/omega0)**2.0) * 
    np.exp(-.5*(angdist(alpha, delta, alpha0=alpha00, delta0=delta00)/omega00)**2.0) * np.sin(delta))

def overlap_integral(alpha0, alpha00, delta0, delta00, omega0, omega00):
    
    numerator = 4* np.pi*integrate.nquad(overlap_integrand,[[0, 2*np.pi], [0, np.pi]], args=(alpha0,delta0,omega0,alpha00,delta00,omega00))[0]
    denominator = (integrate.nquad(probdensity_integrand,[[0, 2*np.pi], [0, np.pi]], args=(alpha0,delta0,omega0))[0] * 
                   integrate.nquad(probdensity_integrand,[[0, 2*np.pi], [0, np.pi]], args=(alpha00,delta00,omega00))[0])
    
    answer = numerator/denominator
    print('Expected overlap integral: '+str(answer))
    
    

    
# execute tasks
test_combine(tmpdir, 'allsky')
test_combine(tmpdir, 'no-overlap-hemi')
test_combine(tmpdir, 'allsky-hemi')
test_combine(tmpdir, 'same-hemi')
test_combine(tmpdir, 'concentric-gaussians')
test_combine(tmpdir, 'noncoincident-gaussians')
test_combine(tmpdir, 'gaussian-hemi')