# Read h5py files from results

import h5py
import math as mth
import matplotlib.pyplot as plt
import numpy as np


# More simple (and better) cumulative counter
def num_above2(array, minfar, maxfar):
    
    #print(minfar, maxfar)
    powers = np.arange(mth.log10(minfar), mth.log10(maxfar), .1)
    bins = 10.**powers
    
    counts, val = np.histogram(array, bins)
    #digi = np.digitize(array, bins, right=True)
    #val, counts = np.unique(digi, return_counts=True)

    return val, np.cumsum(counts)


def remap_far(far):
    lim = 1e-8
    return np.piecewise(far, [far < lim, far >= lim], [lambda far: far, lambda far: far * (far-lim)**.15 ])


input_path = 'results/run2/'

# Open results
data_counts = h5py.File(input_path+'output-counts.hdf5', 'r')
print(data_counts.keys())
data_far_c = h5py.File(input_path+'output-far_c.hdf5', 'r')
print(data_far_c.keys())
data_spat = h5py.File(input_path+'output-spat_real.hdf5', 'r')
print(data_spat.keys())


# Define constants
years = 100
sec_per_year = 86400 * 365
num_epochs = 1000
num_runs = len(data_far_c.keys())
far_max = np.max(data_far_c['0'][:])
far_min = np.min(data_far_c['0'][:])



# Train remapping model
# Gather data
x_train = []
y_train = []
for ind in range(num_runs):
    i = str(ind)
    array = data_far_c[i][:]/data_spat[i][:]
    for val in array:
        x_train.append(val)
 
X_train = np.sort(np.array(x_train))
Y_train = np.arange(0, 1, 1/len(X_train)) * far_max

def lookupNearest(x0):
    xi = np.abs(X_train-x0).argmin()
    return Y_train[xi]

def correct_far(far_array):
    out_array = []
    for far in far_array:
        out_array.append(lookupNearest(far))
    return np.array(out_array)

Y_trained = correct_far(X_train)

plt.plot(X_train, X_train, '--', color='black', label='expected')
plt.plot(X_train, Y_train, label='data')
plt.plot(Y_train, Y_trained, label='data trained')
plt.xlabel('Input FAR')
plt.ylabel('Output FAR')
plt.xscale('log')
plt.yscale('log')
plt.legend(loc='best')
plt.savefig('test-train.png', bbox_inches='tight', dpi=200)
plt.show()
plt.close()
#print(X_train)


# Load data to plot
far_c = []
far_used_c = []
counts_c = []
far_used_spat = []
counts_spat = []
for ind in range(num_runs):
    i = str(ind)
    print(i)
    far_c = data_far_c[i][:]
    #far_c_spat = remap_far(data_far_c[i][:]/data_spat[i][:])
    far_c_spat = correct_far(data_far_c[i][:]/data_spat[i][:])
    print(far_c_spat)
    coinc_FAR_used_t, coinc_spat_counts_t = num_above2(far_c, minfar=far_min/10., maxfar=far_max*1.02)
    far_used_c.append(coinc_FAR_used_t)
    counts_c.append(coinc_spat_counts_t)
    coinc_FAR_used_spat_t, coinc_spat_counts_spat_t = num_above2(far_c_spat, minfar=far_min/10., maxfar=far_max*1.02)
    far_used_spat.append(coinc_FAR_used_spat_t)
    counts_spat.append(coinc_spat_counts_spat_t)

far_used_c = np.array(far_used_spat)
far_used_spat = np.array(far_used_spat)
counts_c = np.array(counts_c)
counts_spat = np.array(counts_spat)


# Plot Joint FAR
plt.plot(1/far_used_c[0,1:], (far_used_c[0,1:] * sec_per_year * years), '--', color='black', label='Expected')
plt.plot(1/far_used_c[0,1:], np.mean(counts_c, axis=0), label=r'No Sky Maps', alpha=1.)
plt.fill_between(1/far_used_c[0,1:], np.mean(counts_c, axis=0)-np.std(counts_c, axis=0), np.mean(counts_c, axis=0)+np.std(counts_c, axis=0), alpha=.5)
plt.plot(1/far_used_spat[0,1:], np.mean(counts_spat, axis=0), color='orange', label=r'w/ Sky Maps', alpha=1.)
plt.fill_between(1/far_used_spat[0,1:], np.mean(counts_spat, axis=0)-np.std(counts_spat, axis=0), np.mean(counts_spat, axis=0)+np.std(counts_spat, axis=0), alpha=.5)
plt.xscale('log')
plt.yscale('log')
plt.xlabel('IFAR* (sec)')
plt.ylabel('Counts (IFAR > IFAR*)')
plt.xlim(np.min(1/far_used_c[0,1:]*.9), 5e9)
plt.ylim(.3, np.max(counts_c))
#plt.title('Temporal Coincidence')
plt.legend(loc='best')
#['Joint w/o sky maps', 'Joint w/ sky maps', 'Expected']
plt.grid()
#plt.savefig(output_path+'Coincidence_far-untargeted.png', bbox_inches='tight', dpi=200)
plt.title('Untargeted Search')
plt.savefig('test.png', bbox_inches='tight', dpi=200)
plt.show()
plt.close()