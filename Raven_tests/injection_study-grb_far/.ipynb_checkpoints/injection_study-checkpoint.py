#!/usr/bin/env python
# coding: utf-8

# In[1]:

import os
import sys
import glob
import math as mth
import numpy as np
import healpy as hp
import matplotlib.pyplot as plt
import matplotlib as mpl
import timeit
import h5py

mpl.rc('font', size=15)
mpl.rc('figure', figsize=(8, 5))

from astropy.utils.data import download_file
from ligo.gracedb.rest import GraceDb
from ligo.skymap.io.fits import read_sky_map


### Simulate Injections

output_path = 'results/targeted/'

# Try to make output directory
try:
    os.mkdir(output_path)
except:
    print("File already exists")

#sys.stdout = open('output_log.txt','wt')

# Define functions needed
def num_above(array, minfar=10**(-9), maxfar=10**(-3)):
    
    powers = np.arange(mth.log10(minfar), mth.log10(maxfar), .01)
    bins = 10.**powers
    
    digi = np.digitize(array, bins, right=True)
    val, counts = np.unique(digi, return_counts=True)

    return np.array(bins)[val], np.cumsum(counts)

# More simple (and better) cumulative counter
def num_above2(array, minfar, maxfar):
    
    #print(minfar, maxfar)
    powers = np.arange(mth.log10(minfar), mth.log10(maxfar), .1)
    bins = 10.**powers
    
    counts, val = np.histogram(array, bins)
    #digi = np.digitize(array, bins, right=True)
    #val, counts = np.unique(digi, return_counts=True)

    return val, np.cumsum(counts)

def calc_overlap(grb_skymap, lvc_skymap):
    return len(grb_skymap)*grb_skymap.dot(lvc_skymap)/grb_skymap.sum()/lvc_skymap.sum()

def skymap_overlap_integral(exttrig_skymap, se_skymap):
    """ Calculate the sky map overlap integral of the two sky maps. """

    nside_s = hp.npix2nside(len(se_skymap))
    nside_e = hp.npix2nside(len(exttrig_skymap))
    if nside_s > nside_e:
        exttrig_skymap = hp.ud_grade(exttrig_skymap, nside_out=nside_s)
    else:
        se_skymap = hp.ud_grade(se_skymap, nside_out=nside_e)
    se_norm = se_skymap.sum()
    exttrig_norm = exttrig_skymap.sum()
    if se_norm > 0 and exttrig_norm > 0:
        skymap_overlap_integral = (
            np.dot(se_skymap, exttrig_skymap)
            / se_norm / exttrig_norm
            * len(se_skymap))
        return skymap_overlap_integral
    else:
        message = ("RAVEN: ERROR: At least one sky map has a probability "
                   "density that sums to zero or less.")
        return message

def rand_skymap(skymap_array):
    ind = mth.floor(np.random.random() * len(skymap_array))
    return skymap_array[ind]

def mag(x):
    return int(mth.log10(x))


# In[5]:

# Get GRB skymap
print('Loading GRB sky maps.....')
grb_skymap_fnames = glob.glob('data/grb/*.fit')
grb_skymaps_noise = []
grb_skymaps_real = []
for fname in grb_skymap_fnames:
    grb_skymaps_real.append(hp.read_map(fname, verbose=False))
    skymap = np.random.random(4)
    skymap /= skymap.sum()
    grb_skymaps_noise.append(skymap)

# Get LVC skymap
print('Loading LVC sky maps.....')
lvc_skymaps_fnames = glob.glob('data/gw-subthresh/*.fits.gz')
lvc_skymaps_noise = []
lvc_skymaps_real = []
for fname in lvc_skymaps_fnames:
    lvc_skymaps_real.append(hp.read_map(fname, verbose=False))
    skymap = np.random.random(4)
    skymap /= skymap.sum()
    lvc_skymaps_noise.append(skymap)


print('Number of GRB sky maps: '+str(len(grb_skymaps_real)))
print('Number of LVC sky maps: '+str(len(lvc_skymaps_real)))


### Constants

years = 100
sec_per_year = 86400 * 365
OPA_far_thresh = 6 / sec_per_year

gw_far_thresh = 1 / (3600 * 24) #/ 24
n_grb0 = 310 * 0 # per year, "astrophysical number"
grb_far_thresh = 1 / 10000 #/ 10**100
n_grb0_noise = int(grb_far_thresh * sec_per_year) #24 # per year, from GRB noise
# n_grb0_noise / (3600 * 24 * 30 * 12)
grb_rate = (n_grb0) / sec_per_year
n_grb =  int((n_grb0 + n_grb0_noise) * years) # total
n_gw = int(gw_far_thresh * sec_per_year * years) # total

# subthreshold
#sub_ind = np.where(far_gw < 1 / (3600 * 24))[0][0:n_grb0_noise]
#far_grb[sub_ind] = np.random.power(1, n_grb0_noise) * grb_far_thresh
#far_gw[0: n_grb0_noise] = np.random.power(1, 1) * 1/ (3600 * 24)

th = -1 # start window
tl = 10 # end window

print('Simulating '+str(years)+' years')
print('Number of GWs: ' +str(int(n_gw)))
print('Number of GRBs: ' +str(int(n_grb)))
print('Number of real GRBs per yr: ' +str(int(n_grb0)))
print('Number of noise GRBs per yr: ' +str(int(n_grb0_noise)))
print('Using ['+str(th)+', +'+str(tl)+'] window')


#predict number of coincidences
n_err = n_gw * (grb_rate + grb_far_thresh) * (tl-th)

print('Expected number of false coincidence events: ' +str(int(n_err)))
print('Looking for coincidences...')


num_simulations = 30

far_gw_array = []
far_grb_array = []
far_c_array = []
far_c_spat_real = []
far_c_spat_noise = []
spat_real_int_list = []
spat_noise_int_list = []

coinc_FAR_used = []
coinc_FAR_spat_real_used = []
coinc_FAR_spat_noise_used = []
gw_FAR_used = []
grb_FAR_used = []
coinc_counts = []
coinc_spat_real_counts = []
coinc_spat_noise_counts = []
gw_counts = []
grb_counts = []


### Run Simulation ###

for sim in range(num_simulations): 
    # create random time for each event
    t_grb = np.random.random(n_grb) * sec_per_year * years
    t_gw = np.random.random(n_gw) * sec_per_year * years
    
    far_gw = np.random.power(1, n_gw) * gw_far_thresh # create FAR for each GW event
    far_grb = np.random.power(1, n_grb) * grb_far_thresh # create FAR for each GRB event
    far_grb[0:n_grb0] = 0

    # look for coincidences
    num = 0
    i = 0
    far_c_array_t = []
    #far_c_spat_t =[]
    spat_real_int_list_t = []
    spat_noise_int_list_t = []
    t_start = timeit.default_timer()
    Zmax =  gw_far_thresh * (grb_rate + grb_far_thresh) * (tl-th)
    for gw in t_gw:#[0:1000]:
        num_add = np.where((gw - t_grb > th) & (gw - t_grb < tl))[0]
        #print(num_add)
        if num_add.any():
            num += len(num_add)
            
            #1. Untargeted Method
            coinc_far = far_gw[i] * (grb_rate) * (tl-th)
            
            #2. Targeted Method
            #Z = far_gw[i] * far_grb[num_add[0]] * (tl-th)
            #coinc_far = Z - Z * np.log(Z/Zmax)
            
            spat_int_real = skymap_overlap_integral(rand_skymap(grb_skymaps_real), rand_skymap(lvc_skymaps_real))
            spat_int_noise = calc_overlap(rand_skymap(grb_skymaps_noise), rand_skymap(lvc_skymaps_noise))

            far_c_array_t.append(coinc_far)
            spat_real_int_list_t.append(spat_int_real)
            spat_noise_int_list_t.append(spat_int_noise)
        i += 1
    far_c_array_t = np.array(far_c_array_t)
    far_c_spat_real = far_c_array_t / spat_real_int_list_t
    far_c_spat_noise = far_c_array_t / spat_noise_int_list_t
    spat_real_int_real_list_t = np.array(spat_real_int_list_t)
    spat_noise_int_list_t = np.array(spat_noise_int_list_t)
    far_c_array.append(far_c_array_t)
    far_gw_array.append(far_gw)
    far_grb_array.append(far_grb)
    spat_real_int_list.append(spat_real_int_list_t)
    spat_noise_int_list.append(spat_noise_int_list_t)
    
    # count number above each FAR
    coinc_FAR_used_t, coinc_counts_t = num_above2(far_c_array_t, minfar=1/(sec_per_year*years)/10, maxfar=Zmax*1.02)
    coinc_FAR_spat_real_used_t, coinc_spat_real_counts_t = num_above2(far_c_spat_real, minfar=1/(sec_per_year*years)/10, maxfar=Zmax*1.02)
    coinc_FAR_spat_noise_used_t, coinc_spat_noise_counts_t = num_above2(far_c_spat_noise, minfar=1/(sec_per_year*years)/10, maxfar=Zmax*1.02)
    gw_FAR_used_t, gw_counts_t = num_above2(far_gw, minfar=1/(sec_per_year*years)/10, maxfar=gw_far_thresh*1.02)
    grb_FAR_used_t, grb_counts_t = num_above2(far_grb[n_grb0:-1], minfar=1/(sec_per_year*years)/10, maxfar=(grb_rate + grb_far_thresh)*1.02)
    
    #print(coinc_counts_t)
    
    coinc_FAR_used.append(coinc_FAR_used_t)
    coinc_FAR_spat_real_used.append(coinc_FAR_spat_real_used_t)
    coinc_FAR_spat_noise_used.append(coinc_FAR_spat_noise_used_t)
    gw_FAR_used.append(gw_FAR_used_t)
    grb_FAR_used.append(grb_FAR_used_t)
    coinc_counts.append(coinc_counts_t)
    coinc_spat_real_counts.append(coinc_spat_real_counts_t)
    coinc_spat_noise_counts.append(coinc_spat_noise_counts_t)
    gw_counts.append(gw_counts_t)
    grb_counts.append(grb_counts_t)
    
    
    t_end = timeit.default_timer()
    print('Number of found coincidences: ' +str(int(num)))
    print('Took '+str((t_end - t_start)/60)+' minutes')
    

# Convert to numpy arrays
far_c_array = np.array(far_c_array)
spat_real_int_list = np.array(spat_real_int_list)
spat_noise_int_list = np.array(spat_noise_int_list)
far_gw_array = np.array(far_gw_array)
far_grb_array = np.array(far_grb_array)

coinc_FAR_used = np.array(coinc_FAR_used)
coinc_FAR_spat_real_used = np.array(coinc_FAR_spat_real_used)
coinc_FAR_spat_noise_used = np.array(coinc_FAR_spat_noise_used)
gw_FAR_used = np.array(gw_FAR_used)
grb_FAR_used = np.array(grb_FAR_used)
coinc_counts = np.array(coinc_counts)
coinc_spat_real_counts = np.array(coinc_spat_real_counts)
coinc_spat_noise_counts = np.array(coinc_spat_noise_counts)
gw_counts = np.array(gw_counts)
grb_counts = np.array(grb_counts)


### Save Results ###

    
with h5py.File(output_path+'output-counts.hdf5', 'w') as f:
    # Save FAR used
    f.create_dataset('gw_FAR_used', data=gw_FAR_used[0,1:])
    f.create_dataset('grb_FAR_used', data=grb_FAR_used[0,1:]) 
    f.create_dataset('coinc_FAR_used', data=coinc_FAR_used[0,1:])
    f.create_dataset('coinc_FAR_spat_real_used', data=coinc_FAR_spat_real_used[0,1:])
    f.create_dataset('coinc_FAR_spat_noise_used', data=coinc_FAR_spat_noise_used[0,1:])
    # Save counts
    f.create_dataset('gw_counts', data=gw_counts)
    f.create_dataset('grb_counts', data=grb_counts)
    f.create_dataset('coinc_counts', data=coinc_counts)
    f.create_dataset('coinc_spat_real_counts', data=coinc_spat_real_counts)
    f.create_dataset('coinc_spat_noise_counts', data=coinc_spat_noise_counts)
      
with h5py.File(output_path+'output-far_c.hdf5', 'w') as f:
    for i in range(len(far_c_array)):
        f.create_dataset(str(i), data=far_c_array[i])
    
with h5py.File(output_path+'output-spat_real.hdf5', 'w') as f:
    for i in range(len(far_c_array)):
        f.create_dataset(str(i), data=spat_real_int_list[i])

with h5py.File(output_path+'output-spat_noise.hdf5', 'w') as f:
    for i in range(len(far_c_array)):
        f.create_dataset(str(i), data=spat_noise_int_list[i])

### Plot Results ###
        
# Plot GW FAR
plt.errorbar(1/gw_FAR_used[0,1:], np.mean(gw_counts, axis=0), yerr=np.std(gw_counts, axis=0), label='GW Pipline')
plt.plot(1/gw_FAR_used[0,1:], (gw_FAR_used[0,1:] * sec_per_year * years), '--',zorder=1, label='Expected')
plt.xscale('log')
plt.yscale('log')
plt.xlabel('IFAR (s)')
plt.ylabel('Cumulative Count')
plt.title('Gravitational Pipeline')
plt.legend(loc='best')
plt.grid()
plt.savefig(output_path+'Gravitational_far.png', bbox_inches='tight', dpi=200)
plt.show()
plt.close()


# Plot GRB FAR
plt.plot(1/grb_FAR_used[0,1:], np.mean(grb_counts, axis=0), label='GRB Pipeline')
plt.fill_between(1/grb_FAR_used[0,1:], np.mean(grb_counts, axis=0)-np.std(grb_counts, axis=0), np.mean(grb_counts, axis=0)+np.std(grb_counts, axis=0), alpha=.5)
plt.plot(1/grb_FAR_used[0,1:], (grb_FAR_used[0,1:] * sec_per_year * years), '--',zorder=1, label='Expected')
plt.xscale('log')
plt.yscale('log')
plt.xlabel('IFAR* (sec)')
plt.ylabel('Number (IFAR > IFAR*)')
plt.title('Gamma Ray Burst Pipeline')
plt.legend(loc='best')
plt.grid()
plt.savefig(output_path+'Gammaray_far.png', bbox_inches='tight', dpi=200)
plt.show()
plt.close()


# Plot Joint FAR
plt.plot(1/coinc_FAR_used[0,1:], (coinc_FAR_used[0,1:] * sec_per_year * years), '--', color='black', label='Expected')
plt.errorbar(1/coinc_FAR_used[0,1:], np.mean(coinc_counts, axis=0), label=r'No Sky Maps', alpha=1.)
plt.fill_between(1/coinc_FAR_used[0,1:], np.mean(coinc_counts, axis=0)-np.std(coinc_counts, axis=0), np.mean(coinc_counts, axis=0)+np.std(coinc_counts, axis=0), alpha=.5)
plt.plot(1/coinc_FAR_spat_real_used[0,1:], np.mean(coinc_spat_real_counts, axis=0), color='orange', label=r'w/ Sky Maps', alpha=1.)
plt.fill_between(1/coinc_FAR_spat_real_used[0,1:], np.mean(coinc_spat_real_counts, axis=0)-np.std(coinc_spat_real_counts, axis=0), np.mean(coinc_spat_real_counts, axis=0)+np.std(coinc_spat_real_counts, axis=0), color='orange',alpha=.5)
plt.xscale('log')
plt.yscale('log')
plt.xlabel('IFAR* (sec)')
plt.ylabel('Counts (IFAR > IFAR*)')
plt.xlim(np.min(1/coinc_FAR_used[0,1:]*.9), 5e9)
plt.ylim(.3, np.max(coinc_counts))
#plt.title('Temporal Coincidence')
plt.legend(loc='best')
#['Joint w/o sky maps', 'Joint w/ sky maps', 'Expected']
plt.grid()
plt.savefig(output_path+'Coincidence_far-untargeted.png', bbox_inches='tight', dpi=200)
plt.title('Untargeted Search')
plt.savefig(output_path+'Coincidence_far-untargeted-w-title.png', bbox_inches='tight', dpi=200)
plt.show()
plt.close()


spat_real_int_list_2 = [item for sublist in spat_real_int_list for item in sublist]
spat_noise_int_list_2 = [item for sublist in spat_noise_int_list for item in sublist]


# Plot Overlap Integral
bins = 10**np.arange(-7,2,.1)  
plt.hist(spat_real_int_list_2, bins=bins, alpha=.7, color='orange')
plt.axvline(x=np.mean(spat_real_int_list_2), linestyle='--', alpha=.7, label='Mean', color='orange')
plt.xlabel('Sky map overlap')
plt.ylabel('Counts')
plt.xscale('log')
plt.yscale('log')
plt.legend(loc='best')
plt.title('Untargeted Search')
plt.savefig(output_path+'skymap_overlap.png', dpi=200)
plt.close()


# Plot just spatial joint FAR
plt.errorbar(1/coinc_FAR_spat_real_used[0,1:], np.mean(coinc_spat_real_counts, axis=0), yerr=np.std(coinc_spat_real_counts, axis=0), label=r'Joint (w/ Sky Maps)')
plt.plot(1/coinc_FAR_spat_real_used[0,1:], (coinc_FAR_spat_real_used[0,1:] * sec_per_year * years), '--',zorder=1, label='Expected')
plt.xscale('log')
plt.yscale('log')
plt.xlabel('IFAR* (s)')
plt.ylabel('Counts (IFAR > IFAR*)')
#plt.title('Space-time Coincidence')
plt.legend(loc='best')
plt.grid()
plt.savefig(output_path+'Coincidence_spat.png', bbox_inches='tight', dpi=200)
plt.show()
plt.close()


# Plot all FARs together
plt.errorbar(1/gw_FAR_used[0,1:], np.mean(gw_counts, axis=0), label='GW Pipline')
plt.errorbar(1/grb_FAR_used[0,1:], np.mean(grb_counts, axis=0), label='GRB Pipeline')
plt.errorbar(1/coinc_FAR_used[0,1:], np.mean(coinc_counts, axis=0), label=r'Joint (Only Temporal)')
plt.errorbar(1/coinc_FAR_spat_real_used[0,1:], np.mean(coinc_spat_real_counts, axis=0), label=r'Joint (w/ Sky Maps)')
plt.plot(1/coinc_FAR_spat_real_used[0,1:], (coinc_FAR_spat_real_used[0,1:] * sec_per_year * years), '--',zorder=1, label='Expected')
plt.axvline(x=1/OPA_far_thresh, linestyle='-.', label='OPA threshold')
plt.xlim(3600, 1e11)
plt.ylim(1e-1, 1e6)
plt.xscale('log')
plt.yscale('log')
plt.xlabel('IFAR* (sec)')
plt.ylabel('Number (IFAR > IFAR*)')
plt.legend(loc='best')
plt.grid()
plt.savefig(output_path+'all_far.png', bbox_inches='tight', dpi=200)
plt.show()
plt.close()

