# $ ligo-proxy init user.name
# $ python (use python 3.6)

import numpy as np
from lxml import etree
import xml.etree.ElementTree as ET
import tempfile
from ligo.gracedb.rest import GraceDb
from astropy.time import Time

#use GraceDb() or include the respective url if need normal GraceDb

client = GraceDb('https://gracedb-playground.ligo.org/api/')

def create_external_event(gpstime):

    new_TrigID = 123400000+int(np.random.random()*100000)
    new_date = str(Time(gpstime, format='gps', scale='utc').isot)
    print(new_date)

    kwargs = {'mode': 'w+b'}
    with tempfile.NamedTemporaryFile(**kwargs) as xmlfile:
        root = etree.parse(r"/Users/brandonpiotrzkowski/ligogit/Backup_RAVEN_upload/upload_exttrig/initial.data")
        
        root.find("./Who/Date").text = str(new_date).encode()
        root.find("./WhereWhen/ObsDataLocation/ObservationLocation/AstroCoords/Time/TimeInstant/ISOTime").text = str(new_date).encode()
        root.find("./What/Param[@name='TrigID']").attrib['value'] =  str(new_TrigID).encode()

        print(root.xpath("./What/Param[@name='TrigID']")[0].attrib['value'])

        s = etree.tostring(root, xml_declaration=True,encoding="UTF-8",pretty_print=True)
        xmlfile.write(s)

        r = client.createEvent("External", "Fermi", xmlfile.name, search="GRB")
        event_dict = r.json()
        graceid = event_dict["graceid"]
        print(graceid)
        print(r)


gpstime = 1250887606.072754

for i in range(-1, 5):
    create_external_event(gpstime+i)
