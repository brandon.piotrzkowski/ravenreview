import sys
from ligo.raven import gracedb_events, search
from ligo.gracedb.rest import GraceDb

g = GraceDb('https://gracedb-playground.ligo.org/api/') #use GraceDb() or include the respective url if need normal GraceDb
sys.stdout = open('query_window_tests_output.txt','wt')

gpstime = 1248278592.5 # center of the ten external events

# First set of results return everything 
results = search.query('External', gpstime, -10, 10, gracedb=g, group=None)
print('Return all the events')
for result in results:
    print(result['graceid'])
print('---')    
    
# Use window to return certain results
results = search.query('External', gpstime, -3, 3, gracedb=g, group=None)
print('Return within [-3,+3] window, centered in the middle')
for result in results:
    print(result['graceid'])
print('---')    
    
results = search.query('External', gpstime, -4, -1, gracedb=g, group=None)
print('Return within [-4,-1] window, centered in the middle')
for result in results:
    print(result['graceid'])
print('---')    
    
results = search.query('External', gpstime, -1, 5, gracedb=g, group=None)
print('Return within [-1,+5] window, centered in the middle ')
for result in results:
    print(result['graceid'])
