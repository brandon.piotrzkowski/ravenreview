# $ ligo-proxy init user.name
# $ python (use python 3.6)

from ligo.gracedb.rest import GraceDb

#use GraceDb() or include the respective url if need normal GraceDb

client = GraceDb('https://gracedb-playground.ligo.org/api/')

event_file = "initial.data"
r = client.createEvent("External", "Fermi", event_file, search="GRB")
event_dict = r.json()
graceid = event_dict["graceid"]
print(graceid)
print(r)
