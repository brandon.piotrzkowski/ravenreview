# install RAVEN
# $ pip install ligo-raven
# $ ligo-proxy init user.name
# $ python (use python 3.6)

import sys
from ligo.raven import gracedb_events
from ligo.raven import gracedb_events, search
from ligo.gracedb.rest import GraceDb

sys.stdout = open('raven_query_test_output.txt','wt')
#use GraceDb() or include the respective url if need normal GraceDb
g = GraceDb('https://gracedb.ligo.org/api/')  

# same gps time of a burst event https://gracedb-playground.ligo.org/superevents/S190722ad/view/
gpstime = 1247866992.67	    
tl= -6000
th= 6000

# the group 'Burst' should only return superevents with preferred group 'Burst' when used
group = 'Burst' 

# First set of results return everything within time window
print('Show every event in the time window')

results0 = search.query('Superevent', gpstime, tl, th, gracedb=g, group=None)
for superevent in results0:
    preferred_event_id = superevent['preferred_event']
    print(superevent['preferred_event'] , g.event(preferred_event_id).json()['group'])

print('--')

# Second set of results should return only entries with the same group
print('Now using group to only show group="Burst"')

results_group = search.query('Superevent', gpstime, tl, th, gracedb=g, group=group)
for superevent in results_group:
    preferred_event_id = superevent['preferred_event']
    print(superevent['preferred_event'] ,g.event(preferred_event_id).json()['group'])




